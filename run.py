import asyncio
import talon
import config
import discord
import logging
from datetime import datetime

async def send_540():
    await bot.wait_until_ready()
    channel = bot.get_channel(570080602620166144)
    sent = False
    while not bot.is_closed():
        if (datetime.now().hour == 9 or datetime.now().hour == 21) and datetime.now().minute == 40:
            if not sent:
                sent = True
                await channel.send("540")
        elif sent:
            sent = False
        await asyncio.sleep(20)

# logger = logging.getLogger('discord')
# logger.setLevel(logging.INFO)
# handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
# handler.setFormatter(logging.Formatter('%(asctime)s:%(Levelname)s:%(name)s: %(message)s'))
# logger.addHandler(handler)

print("before creation")
bot = talon.Talon()
print("after creation")

bot.loop.create_task(send_540())
bot.run(config.token)
