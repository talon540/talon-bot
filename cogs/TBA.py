import discord
from discord.ext import commands
import requests
import json

class TBACog(commands.Cog, name='The Blue Alliance'):
    def __init__(self, bot):
        self.bot = bot
        self.params = {'X-TBA-Auth-Key': '9jTcQNzhUTyC3bFoSViW4s5as9z96Fw6RgQW7qtYWW8VPsAS0IcUXVCzLjqz3Edi'}
        self.url = ('https://www.thebluealliance.com/api/v3/')
        self.session = requests.Session()


    @commands.command()
    async def team(self, ctx, teamnum:int):
        '''hello there'''
        team = self.session.get(self.url+'team/frc%s' % teamnum, params=self.params).json()
        embed = discord.Embed(title=team['nickname'], description=team['motto'], color=0x00ff00)
        embed.add_field(name='Location:', value='%s, %s, %s' % (team['city'], team['state_prov'], team['country']), inline=True)
        embed.add_field(name='Team since:', value=team['rookie_year'], inline=True)
        embed.add_field(name='Website:', value=team['website'], inline=False)
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(TBACog(bot))
