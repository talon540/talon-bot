import discord
from discord.ext import commands

class TalonHelpCommand(commands.HelpCommand):
    def __init__(self):
        super().__init__()
        self.color = discord.Color.red()

    async def send_bot_help(self, mapping):
        embed = self.create_embed()
        command_name = self.context.invoked_with
        description="Use `{0}{1} <command/category>` for more info.".format(
                self.clean_prefix, command_name)

        for cog, cog_commands in mapping.items():
            filtered = await self.filter_commands(cog_commands)
            if filtered:
                command_list = ''
                for command in filtered:
                    command_list += command.name + ', '
                command_list = command_list[:-2]

                embed.add_field(name=cog.qualified_name, value='```{}```'.format(command_list), inline=False)
        
        destination = self.get_destination()
        await destination.send(embed=embed)

    async def send_command_help(self, command):
        embed = self.create_embed()
        embed.title = command.name
        embed.add_field(name=f'**Usage:** `{command.name} {command.signature}`', value=command.short_doc or "No description")
        destination = self.get_destination()
        await destination.send(embed=embed)

    async def send_group_help(self, group):
        embed = self.create_embed()
        embed.title = group.name
        embed.description = group.help or 'No description'

        filtered = await self.filter_commands(group.commands)
        if filtered:
            for command in filtered:
                embed.add_field(name=command.name, value=command.short_doc or 'No description')

        destination = self.get_destination()
        await destination.send(embed=embed)

    async def send_cog_help(self, cog):
        embed = self.create_embed()
        embed.title = cog.qualified_name
        embed.description = cog.description or 'No description'

        filtered = await self.filter_commands(cog.get_commands())
        if filtered:
            for command in filtered:
                embed.add_field(name=f'**Usage:** `{command.name} {command.signature}`', value=command.short_doc or 'No Description', inline=False)

        destination = self.get_destination()
        await destination.send(embed=embed)

    def command_not_found(self, string):
        return f'Command or category "{string}" not found.'

    def subcommand_not_found(self, command, string):
        return f'Subcommand "{string}" not found.'

    def create_embed(self):
        embed = discord.Embed(color=self.color)
        embed.set_author(
                name=self.context.bot.user.name,
                icon_url='https://static1.squarespace.com/static/5c3a34a64eddec8cad28b92d/t/5c3a406870a6ad9840ba876f/'
        )

        return embed

class GeneralCog(commands.Cog, name='General'):
    def __init__(self, bot):
        self.bot = bot
        self._original_help_command = bot.help_command
        bot.help_command = TalonHelpCommand()
        bot.help_command.cog = self

    @commands.command()
    async def info(self, ctx):
        '''hi'''
        await ctx.send('info')

    def cog_unload(self):
        self.bot.help_command = self._original_help_command

def setup(bot):
    bot.add_cog(GeneralCog(bot))
