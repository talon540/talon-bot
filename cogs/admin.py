import discord
from discord.ext import commands

class AdminCog(commands.Cog, name='Admin'):
    def __init__(self, bot):
        self.bot = bot

    async def bot_check_once(self, ctx):
        blacklist = await self.bot.get_blacklist(ctx.guild.id)
        return ctx.channel.id not in blacklist

    @commands.group(name='prefix', invoke_without_command=True)
    async def prefix(self, ctx):
        '''Manage server custom prefixes.

        When called without any subcommands it will list all the current prefixes
        '''
        prefixes = await self.bot.get_guild_prefixes(ctx.guild)
        del prefixes[0]

        embed = discord.Embed()
        field_val = ''
        for prefix in prefixes:
            field_val += prefix + ', '
        embed.add_field(name='Current Prefixes:', value=str(field_val[:-2]))
        await ctx.send(embed=embed)

    @prefix.command(name='add')
    async def add_prefix(self, ctx, new_prefix:str):
        if len(new_prefix) > 10:
            ctx.send('Cannot have a prefix longer than 10 characters')
            return
        prefixes = await self.bot.get_raw_guild_prefixes(ctx.guild.id)
        if prefixes:
            prefixes.append(new_prefix)
        else:
            prefixes = new_prefix
        await self.bot.set_guild_prefixes(ctx.guild, prefixes)
        await ctx.send('Prefix succesfuly added')

    @prefix.command(name='remove')
    async def remove_prefix(self, ctx, prefix:str):
        prefixes = await self.bot.get_raw_guild_prefixes(ctx.guild.id)
        if prefix not in prefixes:
            ctx.send('Prefix does not exist')
            return
        prefixes.remove(prefix)
        await self.bot.set_guild_prefixes(ctx.guild, prefixes)
        await ctx.send('Prefix succesfuly removed')

    @prefix.command(name='clear')
    async def clear_prefix(self, ctx):
        await self.bot.set_guild_prefixes(ctx.guild, '')
        await ctx.send('Prefixes succesfuly cleared. Mention the bot to use any commands now')

    @commands.group(name='blacklist', invoke_without_command=True)
    async def blacklist(self, ctx):
        '''Manage server blacklisted channels.
        
        When called without any subcommands it will list all currently blacklisted channels
        '''
        channels = await self.bot.get_blacklist(ctx.guild.id)

        embed = discord.Embed()
        field_val = ''
        if not channels:
            field_val = 'No currently blacklisted channels.'
        else:
            for channel in channels:
                channel = self.bot.get_channel(channel)
                field_val += channel.name + ', '
            field_val = field_val[:-2]

        embed.add_field(name='Blacklisted Channels:', value=str(field_val))
        await ctx.send(embed=embed)

    @blacklist.command(name='add')
    async def add_blacklist(self, ctx, channel:discord.TextChannel):
        blacklist = await self.bot.get_blacklist(ctx.guild.id)
        if not blacklist:
            blacklist = []
        blacklist.append(channel.id)
        await self.bot.set_blacklist(ctx.guild, blacklist)
        await ctx.send('Channel succesfuly blacklisted')

    @blacklist.command(name='remove')
    async def remove_blacklist(self, ctx, channel:discord.TextChannel):
        blacklist = await self.bot.get_blacklist(ctx.guild.id)
        if channel.id not in blacklist:
            ctx.send('Channel not in blacklist')
            return
        blacklist.remove(channel.id)
        await self.bot.set_blacklist(ctx.guild, blacklist)
        await ctx.send('Channel succesfuly removed')

    @blacklist.command(name='clear')
    async def clear_blacklist(self, ctx):
        await self.bot.set_blacklist(ctx.guild, '')
        await ctx.send('Blacklist succesfully cleared')

def setup(bot):
    bot.add_cog(AdminCog(bot))
