import asyncio
import asyncpg
import datetime
import discord
from discord.ext import commands
import config

initial_extensions = ['cogs.general',
                      'cogs.admin',
                      'cogs.TBA']


async def _prefix_callable(bot, msg):
    user_id = bot.user.id
    prefixes = [f'<@!{user_id}> ', f'<@{user_id}> ']
    if msg.guild is None:
        prefixes.append('$')
    else:
        async with bot.pool.acquire() as conn:
            guild_prefixes = await conn.fetchval('SELECT prefixes FROM guilds WHERE id=$1', msg.guild.id)
            if guild_prefixes:
                prefixes.extend(guild_prefixes)
        # prefixes.extend(bot.prefixes.get(msg.guild.id, ['$']))
    return prefixes

class Talon(commands.Bot):
    def __init__(self):
        super().__init__(command_prefix=_prefix_callable)
        self.started_on = datetime.datetime.utcnow()
        self.owner = None

        for extension in initial_extensions:
            self.load_extension(extension)

    async def on_connect(self):
        #self.pool = await asyncpg.create_pool(dsn=config.postgres, command_timeout=60)
        print('connected')
        self.pool = await asyncpg.create_pool(user='talonbot', password='#Tal0n540B0t-', command_timeout=60)
        async with self.pool.acquire() as conn:
            exists = await conn.fetchval('''
                SELECT COUNT(*)
                FROM information_schema.tables
                WHERE table_name = 'guilds'
            ''')
            if not exists:
                await conn.execute('''
                    CREATE TABLE guilds(
                        id bigint PRIMARY KEY,
                        owner text,
                        prefixes text[],
                        blacklisted_channels bigint[]
                    )
                ''')
            
            stored_guilds_records = await conn.fetch('''
                SELECT id
                FROM guilds;
            ''')

            stored_guilds = (record['id'] for record in stored_guilds_records)

        new_guilds = []
        for guild in self.guilds:
            if guild.id not in stored_guilds:
                new_guilds.append(guild.id)

        async with self.pool.acquire() as conn:
            for guild in new_guilds:
                await conn.execute('''
                    INSERT INTO guilds(id) VALUES ($1)
                ''', guild)

        if not self.owner:
            info = await self.application_info()
            self.owner = info.owner

    async def on_message(self, message):
        if message.author.bot or not self.is_ready():
            return

        if "540" in message.content and ctx.channel.id == 570080602620166144:
            await message.add_reaction('5⃣')
            await message.add_reaction('4⃣')
            await message.add_reaction('0⃣')

        await self.process_commands(message)

    async def on_guild_join(self, guild):
        async with self.pool.acquire() as conn:
            exists = await conn.fetchval('SELECT count(*) FROM guilds WHERE id = $1', guild.id)
            if not exists:
                await conn.execute('''
                    INSERT INTO guilds(id) VALUES($1)
                ''', guild.id)

    async def on_guild_remove(self, guild):
        async with self.pool.acquire() as conn:
            await conn.execute('''
                DELETE FROM guilds
                WHERE id = $1
            ''', guild.id)

    async def on_command_error(self, context, exception):
        if isinstance(exception, commands.CommandOnCooldown):
            if await self.is_owner(context.author):
                await context.reinvoke()
        elif isinstance(exception, commands.DisabledCommand):
            await ctx.author.send('This command is currently disabled and unusable.')
        elif isinstance(exception, Blacklisted):
            pass
        elif isinstance(exception, commands.CheckFailure):
            pass
        elif isinstance(exception, commands.CommandNotFound):
            pass
        else:
            await super().on_command_error(context, exception)

    async def get_guild_prefixes(self, guild):
        proxy_msg = discord.Object(id=None)
        proxy_msg.guild = guild
        return await _prefix_callable(self, proxy_msg)

    async def get_raw_guild_prefixes(self, guild_id):
        async with self.pool.acquire() as conn:
            prefixes = await conn.fetchval('SELECT prefixes FROM guilds WHERE id=$1', guild_id)
        return prefixes

    async def set_guild_prefixes(self, guild, prefixes):
        if len(prefixes) > 10:
            raise RuntimeError('Cannot have more than 10 custom prefixes.')
        else:
            prefixes = sorted(set(prefixes), reverse=True)
            async with self.pool.acquire() as conn:
                await conn.execute('''
                    UPDATE guilds
                    SET prefixes = $1
                    WHERE id = $2
                ''', prefixes, guild.id)

    async def get_blacklist(self, guild_id):
        async with self.pool.acquire() as conn:
            blacklist = await conn.fetchval('SELECT blacklisted_channels FROM guilds WHERE id=$1', guild_id)
        return blacklist

    async def set_blacklist(self, guild, channels):
        blacklist = sorted(set(channels), reverse=True)

        async with self.pool.acquire() as conn:
            await conn.execute('''
                UPDATE guilds
                SET blacklisted_channels = $1
                WHERE id = $2
            ''', blacklist, guild.id)
